#include <stdio.h>

const float capital_max_invest = 0.02 ;

struct money_management{
  float capital ;
  float invest_max ;
} managment ;

int main(void)
{

  printf("entrez votre capital : ") ;
  scanf("%f", &managment.capital) ;

  managment.invest_max = managment.capital * capital_max_invest ;
  
  printf("vous pouvez investir maximum %.4f de votre capital\n", managment.invest_max) ;

  return 0 ;
}
